//
//  ReciveDetailViewCell.swift
//  Recetario
//
//  Created by MacbookProDSM on 23/5/17.
//  Copyright © 2017 Jesus Navarrete Perez. All rights reserved.
//

import UIKit

class ReciveDetailViewCell: UITableViewCell {

    @IBOutlet weak var keyLabel: UILabel!
    
    @IBOutlet weak var valueLabel: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
