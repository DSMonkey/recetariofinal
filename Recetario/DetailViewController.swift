//
//  DetailViewController.swift
//  Recetario
//
//  Created by MacbookProDSM on 22/5/17.
//  Copyright © 2017 Jesus Navarrete Perez. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var recipeImageView: UIImageView!
    
    
    @IBOutlet var tableView: UITableView!
    
    @IBOutlet var ratingButton: UIButton!
    
 
    
    var recipe : Receta!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.recipeImageView.image = self.recipe.image
        
        
        // Modifica el color de fondo de la tabla " cell.backgroundColor = UIColor.clear en el cellForRowAt indexPath:"
        
        self.tableView.backgroundColor = UIColor(red: 0.9, green: 0.9, blue: 152, alpha: 0.25)
        
        // Elimina las celdas vacias que quedan al final
        
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        
        //Modifica el color de los separadores de las celdas
        
        self.tableView.separatorColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        
        // Establece el autoajuste del alto de las celdas. Se le da una altura estimada y luego se establece en automatico. Solo se aplicará en este ViewController.
        self.tableView.estimatedRowHeight = 60
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        self.ratingButton.setImage(UIImage(named:self.recipe.rating), for: .normal)

    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.hidesBarsOnSwipe = false
        navigationController?.setNavigationBarHidden(false, animated: true)
        
    }
    
    override var prefersStatusBarHidden: Bool{
        return true
    }
    
    //Habilita el UnWin segue para que al pulsar el asta en la pantalla del rating retroceda
    @IBAction func close(segue: UIStoryboardSegue){
        
        if let reviewVC = segue.source as? ReviewViewController{
            
            if let rating = reviewVC.ratingSelected{
                self.recipe.rating = rating
                self.ratingButton.setImage(UIImage(named:self.recipe.rating), for: .normal)
                
            }
            
        }
        
    }
    
    
}

extension DetailViewController : UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
        case 0:
            return 2
        case 1:
            return self.recipe.ingredients.count
        case 2:
            return self.recipe.steps.count
            
        default:
            return 0
        }
        
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DetailRecipeCell", for: indexPath) as! ReciveDetailViewCell
        
        cell.backgroundColor = UIColor.clear
        
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                cell.keyLabel.text = "Nombre: "
                cell.valueLabel.text = self.recipe.name
            case 1:
                
                cell.keyLabel.text = "Tiempo: "
                cell.valueLabel.text = "\(self.recipe.time!) Min"
                
            /*case 2:
                
                cell.keyLabel.text = "Favorita: "
                if self.recipe.isFavourite {
                    cell.valueLabel.text = "Si"
                }else {
                    cell.valueLabel.text = "No"
                    
                }*/
                
            default:
                break
            }
        case 1:
            // Bloque de codigo para pintar los indices de cada elementoen el array
            
            //Meto el contenido del array en la varible index
            let index = self.recipe.ingredients[indexPath.row]
            //Compruebo si el array contiene el string index
            if self.recipe.ingredients.contains(index){
                
                let indice = self.recipe.ingredients.index(of: index)// le cargo el numero del indice a la varible indice
                
                cell.keyLabel.text = String(describing: indice! + 1 ) + " -" //Pinto el indice + 1 y una una "-"
                cell.keyLabel.textAlignment = NSTextAlignment.center// alineo el label al centro.
                cell.keyLabel.textColor = #colorLiteral(red: 0.1200981066, green: 0.5898380876, blue: 1, alpha: 1)
            }
            
            cell.valueLabel.text = self.recipe.ingredients[indexPath.row]
            
        case 2:
            
            let index = self.recipe.steps[indexPath.row]
            
            if self.recipe.steps.contains(index){
                
                let indice = self.recipe.steps.index(of: index)
                
                cell.keyLabel.text = String(describing: indice! + 1 ) + " -"
                cell.keyLabel.textAlignment = NSTextAlignment.center
                 cell.keyLabel.textColor = #colorLiteral(red: 0.1200981066, green: 0.5898380876, blue: 1, alpha: 1)
            }

            cell.valueLabel.text = self.recipe.steps[indexPath.row]
        default:
            break
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        var title = ""
        
        switch  section {
        case 1:
            title = "Ingredientes: " + String(self.recipe.ingredients.count)
        case 2:
            
            title = "Pasos: " + String(self.recipe.steps.count)
        default:
            break
        }
        return title
    }
    
}

extension DetailViewController  : UITableViewDelegate{
    
}
