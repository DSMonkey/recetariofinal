//
//  ViewController.swift
//  Recetario
//
//  Created by MacbookProDSM on 8/5/17.
//  Copyright © 2017 Jesus Navarrete Perez. All rights reserved.
// Commited desde: IMac Añadidos los iconos escalados


import UIKit

class ViewController: UITableViewController {
    
    var Recetas : [Receta] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        var receta = Receta(name: "Tortilla de patatas",
                            image : #imageLiteral(resourceName: "tortilla"),
                            time: 35,
                            ingredients: ["Patatas","Cebolla","Huevos"],
                            steps: ["Freir las patatas","Pochar la cebolla", "Batir los huevos","Mezclar todo y cuajar los huevos"])
        Recetas.append(receta)
        
        
        receta = Receta(name: "Pizza margarita",
                        image: #imageLiteral(resourceName: "pizza"),
                        time: 25,
                        ingredients: ["Masa pizza","Tomate","Queso","Jamon cocido"],
                        steps: ["Estender la masa","Estender el tomate","Esparcir el queso","Añadir el jamos cocido","Hornear 15 minutos a 180 grados"])
        Recetas.append(receta)
        
        
        receta = Receta(name: "Hamburgesa con queso",
                        image: #imageLiteral(resourceName: "hamburguesa"),
                        time: 15,
                        ingredients: ["Pan de hamburguesa","Cebolla","Carne picada","Queso","Albahaca freca"],
                        steps: ["Asar la carne a la parrilla","Fundir el queso sobre la hamburguesa", "Pochar la cebolla y cubrir el queso","Añadir uns hojas de albahaca"])
        Recetas.append(receta)
        
        
        receta = Receta(name: "Ensalada cesar",
                        image: #imageLiteral(resourceName: "ensalada"),
                        time: 15,
                        ingredients: ["Vegetales","Pollo","Salsa Cesar"],
                        steps: ["Mezclar los vegetales","Freir el pollo y mezclar con los vejetales","Añadir la salsa cesar"])
        Recetas.append(receta)
        
        
        receta = Receta(name: "Paella de marisco",
                        image: #imageLiteral(resourceName: "paella-1167973_1280"),
                        time: 40,
                        ingredients: ["Agua","Gambas","Almejas","Dados de sepia","Arroz","Sal","Azafran","Pimienta"],
                        steps: ["Dorar el marisco y mezclar con el arroz","Añadir el Agua","Salpimentar","4.Añadir el azafran"])
        Recetas.append(receta)
        
        receta = Receta(name: "Sushi",
                        image: #imageLiteral(resourceName: "sushi-2000239_1280"),
                        time: 50,
                        ingredients: ["Agua","Arroz","Sal","Vinagre de arroz","Alga Nori","Salmon", "Atun Rojo"],
                        steps: ["Hervir el arroz","Remojar el alga","Añadir los pescados en la parte central","Enrollar el forma de sushi"])
           
        Recetas.append(receta)
        
        receta = Receta(name: "Sashimi",
                        image: #imageLiteral(resourceName: "tuna-1957234_1280"),
                        time: 15,
                        ingredients: ["Salmon","Atun Rojo","Palometa"],
                        steps: ["Cortar el atun rojo en laminas","Cortar el salmon en laminas","Cortar la palometa en laminas"])
        Recetas.append(receta)
        
    
        
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.hidesBarsOnSwipe = true
        
        
    }

    
    override var prefersStatusBarHidden: Bool{ // funcion que esconde la barra sumerior del dispositivo
        return true
    }
    
    
    
    //MARK: - UITableViewDataSorce
    
    
     //Funciones que define la tabla, las secciones y las celtas
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.Recetas.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let receta = Recetas[indexPath.row]
        let cellId = "RecetaCell"
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! RecipeCell //indica la clase de celda
       
        cell.thumbnailImageView.image = receta.image
        cell.nameLabel.text = receta.name
        cell.timeLabel.text = "\(receta.time!) min"
        cell.ingredientsLabel.text = "Ingredientes: \(receta.ingredients.count)"
        
        // Para hacer las imagenes circulares - tambien se puede hacer por storyboard
        
        //cell.thumbnailImageView.layer.cornerRadius = 42.0
        //cell.thumbnailImageView.clipsToBounds = true
        
        // Para hacer las imagenes con bordes
        cell.thumbnailImageView.layer.borderWidth = 1
        cell.thumbnailImageView.layer.borderColor = (UIColor.black).cgColor
        
        // Para hacer las imagenes con sombra
        //cell.thumbnailImageView.layer.shadowColor = (UIColor.black).cgColor
        //cell.thumbnailImageView.layer.shadowOffset = CGSize(width: 10, height: 5)
        //cell.thumbnailImageView.layer.shadowOpacity = 0.5
        
        
        //if que pone o quita el checmark si la variable isFavourite es true
       /* if receta.isFavourite {
            cell.accessoryType = .checkmark
        }else {
            cell.accessoryType = .disclosureIndicator
        }*/
        
        return cell
        
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) { // esta funcion habilita la edicion de las celdas con el swipe lateral
        
        if editingStyle == .delete{
            self.Recetas.remove(at: indexPath.row)//elimino la receta de este indexpath
        }
        self.tableView.deleteRows(at: [indexPath], with: .fade)// repinta la tabla eliminando la celda borrada con una animacion, se usa para no repintar la tabla completa.
        
    }
    // Compartir......
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let shareAction = UITableViewRowAction(style: .default, title: "Compartir") { (Action, indexPath) in
            let shareDefaultText = "Estoy mirando la receta \(self.Recetas[indexPath.row].name!) en la aplicación de recetas fantasticas del curso de Swift 3"
            
            let activityController = UIActivityViewController(activityItems: [shareDefaultText, self.Recetas[indexPath.row].image], applicationActivities: nil)
            
            self.present(activityController, animated: true, completion: nil)
        }
        // Establece el color del boton
        shareAction.backgroundColor = UIColor(colorLiteralRed: 30.0/255.0, green: 164.0/255.0, blue: 253.0/255.0, alpha: 1.0)
        
        // Borrar .....
        
        let deleteAction = UITableViewRowAction(style: .default, title: "Borrar") { (action, indexPath) in
            self.Recetas.remove(at: indexPath.row)
            self.tableView.deleteRows(at: [indexPath], with: .fade)
        }
          // Establece el color del boton
        deleteAction.backgroundColor = UIColor(colorLiteralRed: 202.0/255.0, green: 202.0/255.0, blue: 202.0/255.0, alpha: 1.0)
        
        return [shareAction, deleteAction]
    }
    
    //MARK: - UITableViewDelegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
       /* let recipe = self.Recetas[indexPath.row]// Recupero el indice para poder usar el recipe.name en el titulo de la alerta.
        
        //Controlador de la alerta
        let alertController = UIAlertController(title: recipe.name, message: "Valora este plato", preferredStyle: .alert)
        
        //Botones de la alerta
        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
        
        var favouriteActionTitle = "Me gusta"
        var favouriteActionStyle = UIAlertActionStyle.default
        
        if recipe.isFavourite {
            favouriteActionTitle = "No me gusta"
            favouriteActionStyle = UIAlertActionStyle.destructive
        }
        
        let favouriteAction = UIAlertAction(title: favouriteActionTitle, style: favouriteActionStyle) { (action) in
           
            
            let recipe = self.Recetas[indexPath.row] //Recuperamos el indice de la receta selecionada
            
            recipe.isFavourite = !recipe.isFavourite // Le decimos de la receta selecionada es igual a lo contrario. Es dedir: si esta selecionada la deselecciona y viceversa
            self.tableView.reloadData()// refrescamos toda la informacion de la tabla
        }
        
        //Conecta los botones con la alerta
        alertController.addAction(cancelAction)
        alertController.addAction(favouriteAction)
        
        //Presenta la alerta
        self.present(alertController, animated: true, completion: nil)*/
        
        
    }
   
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "showRecipeDetail"{
        
            if let indexPath = self.tableView.indexPathForSelectedRow{
                let selectedRecipe = self.Recetas[indexPath.row]
                let destinatioViewController = segue.destination as! DetailViewController
                destinatioViewController.recipe = selectedRecipe
            }
        }
    }

}

