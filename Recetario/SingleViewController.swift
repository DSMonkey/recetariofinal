//
//  SingleViewController.swift
//  Recetario
//
//  Created by MacbookProDSM on 9/5/17.
//  Copyright © 2017 Jesus Navarrete Perez. All rights reserved.
//

import UIKit

class SingleViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var Recetas : [Receta] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /*self.tableView.dataSource = self
        self.tableView.delegate = self as! UITableViewDelegate*/ //codigo para referenciar el datasource y el delegate por codigo. yo lo he hecho graficamente.
        
        var receta = Receta(name: "Tortilla de patatas",
                            image : #imageLiteral(resourceName: "tortilla"),
                            time: 35,
                            ingredients: ["Patatas","Cebolla","Huevos"],
                            steps: ["1.Freir las patatas","2.Pochar la cebolla", "3.Batir los huevos","4.Mezclar todo y cuajar los huevos"])
        Recetas.append(receta)
        
        
        receta = Receta(name: "Pizza margarita",
                        image: #imageLiteral(resourceName: "pizza"),
                        time: 25,
                        ingredients: ["Masa pizza","Tomate","Queso","Jamon cocido"],
                        steps: ["1.Estender la masa","2.Estender el tomate","3.Esparcir el queso","4.Añadir el jamos cocido","5.Hornear 15 minutos a 180 grados"])
        Recetas.append(receta)
        
        
        receta = Receta(name: "Hamburgesa con queso",
                        image:#imageLiteral(resourceName: "hamburguesa"),
                        time: 15,
                        ingredients: ["Pan de hamburguesa","Cebolla","Carne picada","Queso","Albahaca freca"],
                        steps: ["1.Asar la carne a la parrilla","2.Fundir el queso sobre la hamburguesa", "3.Pochar la cebolla y cubrir el queso","4.Añadir uns hojas de albahaca"])
        Recetas.append(receta)
        
        
        receta = Receta(name: "Ensalada cesar",
                        image: #imageLiteral(resourceName: "ensalada"),
                        time: 15,
                        ingredients: ["Vegetales","Pollo","Salsa Cesar"],
                        steps: ["1.Mezclar los vegetales","2.Freir el pollo y mezclar con los vejetales","3.Añadir la salsa cesar"])
        Recetas.append(receta)
        
        receta = Receta(name: "Paella de marisco",
                        image: #imageLiteral(resourceName: "paella-1167973_1280"),
                        time: 40,
                        ingredients: ["Agua","Gambas","Almejas","Dados de sepia","Arroz","Sal","Azafran","Pimienta"],
                        steps: ["1.Dorar el marisco y mezclar con el arroz","2.Añadir el Agua","3.Salpimentar","4.Añadir el azafran"])
        Recetas.append(receta)
        
        receta = Receta(name: "Sushi",
                        image: #imageLiteral(resourceName: "sushi-2000239_1280"),
                        time: 50,
                        ingredients: ["Agua","Arroz","Sal","Vinagre de arroz","Alga Nori","Salmon", "Atun Rojo"],
                        steps: ["1.Hervir el arroz","2.Remojar el alga","3.Añadir los pescados en la parte central","4.Enrollar el forma de sushi"])
        
        Recetas.append(receta)
        
        receta = Receta(name: "Sashimi",
                        image: #imageLiteral(resourceName: "tuna-1957234_1280"),
                        time: 15,
                        ingredients: ["Salmon","Atun Rojo","Palometa"],
                        steps: ["1.Cortar el atun rojo en laminas","2.Cortar el salmon en laminas","3.Cortar la palometa en laminas"])
        Recetas.append(receta)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override var prefersStatusBarHidden: Bool{ // funcion que esconde la barra sumerior del dispositivo
        return true
    }


}
extension SingleViewController : UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.Recetas.count
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let receta = Recetas[indexPath.row]
        let cellId = "RecetaCell"
        let cell = self.tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! FullRecipeCell
       cell.fullName.text = receta.name
        cell.fullImage.image = receta.image
        return cell
        
    }

}

