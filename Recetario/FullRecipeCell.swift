//
//  FullRecipeCell.swift
//  Recetario
//
//  Created by Jesus Navarrete Perez on 11/5/17.
//  Copyright © 2017 Jesus Navarrete Perez. All rights reserved.
//

import UIKit


class FullRecipeCell: UITableViewCell {

    @IBOutlet var fullImage: UIImageView!
    
    @IBOutlet var fullName: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
