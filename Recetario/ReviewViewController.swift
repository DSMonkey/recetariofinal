//
//  ReviewViewController.swift
//  Recetario
//
//  Created by Jesus Navarrete Perez on 31/5/17.
//  Copyright © 2017 Jesus Navarrete Perez. All rights reserved.
//

import UIKit

class ReviewViewController: UIViewController {

    @IBOutlet var backgroundImageView: UIImageView!
    
    @IBOutlet var ratingStackView: UIStackView!
    
    var ratingSelected : String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //******Todo este bloque es para difuminr la imagen del rating page
        let blurEffect = UIBlurEffect(style: .light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        backgroundImageView.addSubview(blurEffectView)
        
    //*********************
        
        //Define el escalado dela animacion
        let scale = CGAffineTransform(scaleX: 0.0, y: 0.0)
        //Define la translacion de la animacion
        let translation = CGAffineTransform(translationX: 0.0, y: 500.0)
        
        //Define la definicion con una concatenacion de las dos animaciones scala y translacion
        ratingStackView.transform = scale.concatenating(translation)
        
       
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        /*Este bloque anima el escalado dela estack view del rating
        
         UIView.animate(withDuration: 0.5, delay: 0.0, options: [], animations: {
            self.ratingStackView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }, completion: nil)*/
        
        
        //Este bloque anima el rebote de la stack view del rating
        UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.3, initialSpringVelocity: 0.5, options: [], animations: {
            self.ratingStackView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }, completion: nil)
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
    }
    // voy por aqui********************
    @IBAction func ratingPress(_ sender: UIButton) {
        
        switch sender.tag {
        case 1:
            ratingSelected = "dislike"
        case 2:
            ratingSelected = "good"
        case 3:
            ratingSelected = "great"
        default:
            break
        }
        performSegue(withIdentifier: "unwindToDetailView", sender: sender)
        
    }

    
    
   

}
